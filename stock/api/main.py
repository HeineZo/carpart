from fastapi import FastAPI, HTTPException
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from pymongo import MongoClient
from pydantic import BaseModel
from dotenv import load_dotenv
import os
from bson.objectid import ObjectId

# Charger les variables d'environnement depuis le fichier .env
load_dotenv()

app = FastAPI()

# Connexion à la base de données MongoDB
client = MongoClient(os.getenv("MONGODB_URL"))
db = client[os.getenv("MONGODB_NAME")]
collection = db["Products"]


class Product(BaseModel):
    name: str
    description: str
    quantity: int

# Ajout d'un nouveau produit
@app.post("/add")
async def add_product(product: Product):
    product_data = jsonable_encoder(product)
    result = collection.insert_one(product_data)
    return JSONResponse(content={"message": "Le produit a été ajouté avec succès", "id": str(result.inserted_id)})

# Modification d'un produit existant
@app.put("/update/{product_id}")
async def update_product(product_id: str, quantity: int):
    result = collection.update_one({"_id": ObjectId(product_id)}, {"$set": {"quantity": quantity}})
    if result.modified_count == 1:
        return JSONResponse(content={"message": "Le produit a été mis à jour avec succès"})
    else:
        raise HTTPException(status_code=404, detail="Produit introuvable")


# Suppression d'un produit
@app.delete("/delete/{product_id}")
async def delete_product(product_id: str):
    result = collection.delete_one({"_id": ObjectId(product_id)})
    if result.deleted_count == 1:
        return JSONResponse(content={"message": "Le produit a été supprimé avec succès"})
    else:
        raise HTTPException(status_code=404, detail="Produit introuvable")

# Récupérer un produit précis
@app.get("/products/{product_id}")
async def get_product(product_id: str):
    product = collection.find_one({"_id": ObjectId(product_id)})
    if product:
        return JSONResponse(content={"name": product["name"], "description": product["description"],
                                     "quantity": product["quantity"]})
    else:
        raise HTTPException(status_code=404, detail="Produit introuvable")

# Récupérer tous les produits
@app.get("/")
async def get_product():
    products = collection.find()
    product_list = []
    for product in products:
        product_list.append({
            "id": str(product["_id"]),
            "name": product["name"],
            "description": product["description"],
            "quantity": product["quantity"],
        })
    return product_list
