from fastapi import FastAPI, HTTPException, Depends
from mysql.connector import connect, Error
from dotenv import load_dotenv
import os

# Charger les variables d'environnement depuis le fichier .env
load_dotenv()

app = FastAPI()

# Connexion à la base de données MySQL
def connect_to_db():
    try:
        connection = connect(
            host=os.getenv("MYSQL_HOST"),
            user=os.getenv("MYSQL_USER"),
            password=os.getenv("MYSQL_PASSWORD"),
            database=os.getenv("MYSQL_DATABASE")
        )
        return connection
    except Error as e:
        print(f"Impossible de se connecter à la base de données: {e}")
        return None

# Ajouter un client
@app.post("/add")
async def add_client(nom: str, prenom: str, mail: str, nbCommande: int):
    connection = connect_to_db()
    if connection:
        try:
            cursor = connection.cursor()
            query = "INSERT INTO clients (nom, prenom, email, nbCommande) VALUES (%s, %s, %s, %s)"
            values = (nom, prenom, mail, nbCommande)
            cursor.execute(query, values)
            connection.commit()
            return {"message": "Le client a bien été ajouté"}
        except Error as e:
            return HTTPException(status_code=500, detail=f"Erreur lors de l'ajout du client: {e}")
        finally:
            cursor.close()
            connection.close()
    else:
        return HTTPException(status_code=500, detail="Impossible de se connecter à la base de données")

# Modifier un client
@app.put("/update/{idClient}")
async def update_client(idClient: int, nbCommande: int):
    connection = connect_to_db()
    if connection:
        try:
            cursor = connection.cursor()
            query = "UPDATE clients SET nbCommande = %s WHERE id = %s"
            values = (nbCommande, idClient)
            cursor.execute(query, values)
            connection.commit()
            return {"message": "Le client a été mis à jour"}
        except Error as e:
            return HTTPException(status_code=500, detail=f"Erreur lors de la mise à jour du client: {e}")
        finally:
            cursor.close()
            connection.close()
    else:
        return HTTPException(status_code=500, detail="Impossible de se connecter à la base de données")

# Supprimer un client
@app.delete("/delete/{idClient}")
async def delete_client(idClient: int):
    connection = connect_to_db()
    if connection:
        try:
            cursor = connection.cursor()
            query = "DELETE FROM clients WHERE id = %s"
            values = (idClient,)
            cursor.execute(query, values)
            connection.commit()

            if cursor.rowcount > 0:
                return {"message": "Le client a été correctement supprimé"}
            else:
                raise HTTPException(status_code=404, detail="Client introuvable")
        except Error as e:
            return HTTPException(status_code=500, detail=f"Erreur lors de la suppression du client: {e}")
        finally:
            cursor.close()
            connection.close()
    else:
        return HTTPException(status_code=500, detail="Impossible de se connecter à la base de données")

# Récupérer tous les clients
@app.get("/")
async def get_all_clients():
    connection = connect_to_db()
    if connection:
        try:
            cursor = connection.cursor()
            query = "SELECT * FROM clients"
            cursor.execute(query)
            clients = cursor.fetchall()

            if clients:
                clients_info = []
                for client in clients:
                    client_info = {
                        "Identifiant": client[0],
                        "Nom": client[1],
                        "Prénom": client[2],
                        "Adresse mail": client[3],
                        "Nombre de commandes": client[4]
                    }
                    clients_info.append(client_info)
                return clients_info
            else:
                return []
        except Error as e:
            return HTTPException(status_code=500, detail=f"Erreur lors de la récupération des clients: {e}")
        finally:
            cursor.close()
            connection.close()
    else:
        return HTTPException(status_code=500, detail="Impossible de se connecter à la base de données")

# Récupérer les données d'un client
@app.get("/{client_id}")
async def get_client(client_id: int):
    connection = connect_to_db()
    if connection:
        try:
            cursor = connection.cursor()
            query = "SELECT * FROM clients WHERE id = %s"
            values = (client_id,)
            cursor.execute(query, values)
            client = cursor.fetchone()

            if client:
                client_info = {
                    "id": client[0],
                    "nom": client[1],
                    "prenom": client[2],
                    "email": client[3],
                    "nbCommande": client[4]
                }
                return client_info
            else:
                raise HTTPException(status_code=404, detail="Client introuvable")
        except Error as e:
            return HTTPException(status_code=500, detail=f"Erreur lors de la récupération des données du client: {e}")
        finally:
            cursor.close()
            connection.close()
    else:
        return HTTPException(status_code=500, detail="Impossible de se connecter à la base de données")