<h1 align="center">
    Carpart
</h1>

<h4 align="center">API et bases de données créées avec Docker</h4>

<p align="center">
  <a href="#✨-roadmap">Roadmap</a> •
  <a href="#🛠️-lancer-l'application">Démarrer</a> •
  <a href="#🕹️-outils-utilisés">Outils</a> •
  <a href="#🔗-liens-utiles">Liens utiles</a> •
  <a href="#👋-me-contacter">Contact</a>
</p>

> **NOTE:** Ce projet est réalisé dans le cadre d'un projet à l'IUT de Vannes.

## ✨ Roadmap

- [x] Créer un Dockerfile pour chaque API ✅
- [x] Créer un Dockerfile pour chaque base de données ✅
- [x] Créer les routes des API ✅
- [x] Créer un fichier docker-compose général ✅
- [x] Push le projet dans un gitlab registry ✅
- [x] Push le projet sur Dockerhub ✅
- [x] Créer une pipeline pour tout faire automatiquement ✅

## 🛠️ Lancer l'application
> 💡 Au préalable, assurez-vous d'avoir installé et lancé **Docker**

Récupérez les lignes du fichier `.env.exemple` et collez-les dans un fichier que vous nommerez `.env`. 
Remplissez chaque ligne par vos informations de connexion aux bases de données.

Depuis la racine du projet:

```bash
# Construire les images
$ docker-compose build

# Lancer le conteneur créé
$ docker-compose up
```

Pour accéder au panel d'administration des différentes API:

- API Client: https://localhost:80/docs
- API Stock produit: https://localhost:81/docs


## 🕹️ Outils utilisés

<p align="center">
   <img src="https://skillicons.dev/icons?i=docker,python,mongo,mysql" alt="Les technologies utilisées" />
</p>

## 🔗 Liens utiles
<div style="display: flex; align-items: center; gap: 10px">
   <a href="https://gitlab.com/HeineZo/carpart">
      <img src="https://skillicons.dev/icons?i=gitlab" width="50" alt="Logo Gitlab" />
   </a>
   <a href="https://gitlab.com/HeineZo/carpart">Voir le projet sur Gitlab</a>
</div>
<br/>
<div style="display: flex; align-items: center; gap: 10px;">
   <a href="https://hub.docker.com/repository/docker/heinezo/carpart/general">
      <img src="https://skillicons.dev/icons?i=docker" width="50" alt="Logo Docker" />
   </a>
   <a href="https://hub.docker.com/repository/docker/heinezo/carpart/general">Voir le projet sur DockerHub</a>
</div>


## 👋 Me contacter
<div style="display: flex; align-items: center; gap: 10px;">
   <a href="https://discord.com/invite/enzolefrigo">
      <img src="https://skillicons.dev/icons?i=discord" width="50" alt="Logo Discord" />
   </a>
   <a href="https://discord.com/invite/enzolefrigo">
      @enzolefrigo
   </a>
</div>

